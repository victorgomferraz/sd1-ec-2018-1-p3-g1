import express from 'express';
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser';
import mainController, {getMessagensByUser, registerAndSendMessageByUser,exitChat} from "./controllers/MainController";
import loginController from "./controllers/LoginController";
import AmqpService from "./service/AmqpService";

AmqpService.instance; //Iniciar a isntancia

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(__dirname + '/web/public'));


app.post('/login', loginController);
app.get('/', mainController);
app.get('/obter_mensagem/:timestamp', getMessagensByUser);
app.post('/gravar_mensagem', registerAndSendMessageByUser);
app.post('/sair', exitChat);

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});