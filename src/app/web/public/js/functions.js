
function addMessageReceived(elemento_id, from, mensagem, timestamp) {
    $('#' + elemento_id).append(createConversationReceivedCard(from, mensagem, timestamp));
}

function addMessageSent(elemento_id, mensagem, timestamp) {
    $('#' + elemento_id).append(createConversationSentCard(mensagem, timestamp));
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date(timestamp) {
    var date = new Date(timestamp);
    var hours = date.getHours();
    var s_hours = hours < 10 ? "0" + hours : "" + hours;
    var minutes = date.getMinutes();
    var s_minutes = minutes < 10 ? "0" + minutes : "" + minutes;
    var seconds = date.getSeconds();
    var s_seconds = seconds < 10 ? "0" + seconds : "" + seconds;
    return s_hours + ":" + s_minutes + ":" + s_seconds;
}

//a função iniciar é onload. isso significa que a carrega_mensagem vai executar o tempo todo,
//e consequentemente a addMessageReceived também
function iniciar(elemento_id) {
    atualizaEstado(false);
    carrega_mensagens(elemento_id, 0);
}

function  atualizaEstado(conectado){
    let txt = conectado ? "Conectado" : "Conectando...";
    $("#status").text(txt+" - irc://" +
        Cookies.get("nick") + "@" +
        Cookies.get("servidor") + "/" +
        Cookies.get("canal"));

    $('#sendMessage').prop('disabled', !conectado);
    $('#sendMessage').prop('placeholder', conectado ? 'Escreva sua mensagem aqui...': 'Estabelecendo conexão, aguarde...');
}

/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp = "0";

function carrega_mensagens(elemento_id, timestamp) {
    var mensagem = "";
    var horario = "";

    $.get("obter_mensagem/" + timestamp, function (data, status) {

        if (status == "success") {
            var linhas = data.messages;
            for (var i = linhas.length - 1; i >= 0; i--) {
                horario = timestamp_to_date(linhas[i].timestamp);
                mensagem = "[" + horario + " - " + linhas[i].nick + "]: " + linhas[i].msg;
                novo_timestamp = linhas[i].timestamp;
                if (Cookies.get("nick") === linhas[i].nick) {
                    addMessageSent(elemento_id, linhas[i].msg, novo_timestamp);
                } else {
                    addMessageReceived(elemento_id, linhas[i].nick, linhas[i].msg, novo_timestamp);
                }
            }
            if(data.nicknames){
                addUsers(data.nicknames);
            }

            atualizaEstado(data.isConnected)
        }
        else {
            alert("erro: " + status);
        }
    });

    t = setTimeout(function () {
        carrega_mensagens(elemento_id, novo_timestamp)
    }, 1000);
}

const addUsers= function (nicknames) {
    var list = $('#user-list .list-group');
    list.empty();
    nicknames.forEach((nickname) => {
        list.append($('<a>', {
            class: "list-group-item",
            id: nickname,
            href: "#"
        }).append($('<span>', {
                class: "connection-status connected"
            }))
            .append(nickname));
    });
};


/*
   Submete a mensagem dos valores contidos s elementos identificados 
   como <elem_id_nome> e <elem_id_mensagem>
*/

//mensagem vem do campo do html pra cá, e daqui vai pro /gravar_mensagem 
function submete_mensagem(elem_id_mensagem) {
    var mensagem = document.getElementById(elem_id_mensagem).value;
    var msg = '{"timestamp":' + Date.now() + ',' + '"nick":"' + Cookies.get("nick") + '",' + '"msg":"' + mensagem + '"}';

    $.ajax({
        type: "post",
        url: "/gravar_mensagem",
        data: msg,
        success:
            function (data, status) {
                if (status == "success") {
                    console.log('entrou no submete_mensagem -> /gravar_mensagem: success!');
                }
                else {
                    alert("erro:" + status);
                }
            },
        contentType: "application/json",
        dataType: "json"
    });
}

function sairChat(elem_id_mensagem) {
    $.ajax({
        type: "post",
        url: "/sair",
        complete: () => {
            location.reload();
        },
        contentType: "application/json",
        dataType: "json"
    });
}

function trocarMode(elemento) {

    var usuario = Cookies.get("nick");
    var args = $("#" + elemento).val();
    var comando = "mode/" + usuario + "/" + args;

    $.get(comando, function (data, status) {
        if (status == "success") {
            alert(comando);
        }
    });
}

const createConversationSentCard = function (message, timestamp) {
    return $('<div>', {
        class: "card sent"
    })
        .append($('<div>', {
            class: "sender",
            text: 'Eu: ' + timestamp_to_date(timestamp)
        }))
        .append($('<div>', {
            class: "message",
            text: message
        }));
};

const createConversationReceivedCard = function (from, message, timestamp) {
    return $('<div>', {
        class: "card received"
    })
        .append($('<div>', {
            class: "sender",
            text: from + ' : ' + timestamp_to_date(timestamp)
        }))
        .append($('<div>', {
            class: "message",
            text: message
        }));
};