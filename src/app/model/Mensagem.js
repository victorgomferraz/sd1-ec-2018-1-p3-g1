import User from "./User";

export default class Mensagem {
    constructor(nick = null, msg = null, timestamp = Date.now(), canal = null) {
        let user = new User();
        user.nick = nick;

        this.timestamp = timestamp;
        this.user = user;
        this.msg = msg;
        this.canal = canal;
    }

    toJson() {
        return this.user ? {
            id: this.user.id,
            timestamp: this.timestamp,
            nick: this.user.nick,
            canal: this.canal ? this.canal.nome : null,
            msg: this.msg
        } : {}
    }
}