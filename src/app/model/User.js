import Mensagem from "./Mensagem";

export default class User {
    constructor(id = null, servidor = null, nick = null, canal = null) {
        this.id = id;
        this.servidor = servidor;
        this.nick = nick;
        this.canal = canal;
        this.connected = false;

        this.mensagens = [];
    }

    addMensagemByNickMsgAndTimestamp(nick, msg, timestamp, canal) {
        console.log(nick, msg, timestamp, canal);
        const message = new Mensagem(nick, msg, timestamp, canal);
        this.mensagens.push(message);
        return message;
    }

    sendMessage(msg) {
        const message = new Mensagem();
        message.msg = msg;
        message.user = this;
        message.canal = this.canal;
        this.addMessage(message);
        return message;
    }

    addMessage(message) {
        this.mensagens.push(message);
    }

    toJsonWithoutMessagens() {
        const {id, servidor, nick, canal, connected} = this;
        return {
            id: id,
            servidor: servidor,
            nick: nick,
            canal: canal.nome,
            connected: connected
        }
    }

}