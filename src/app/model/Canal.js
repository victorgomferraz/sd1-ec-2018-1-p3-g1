export default class Canal {
    constructor(nome = null) {
        this.nome = nome;
        this.nicknames = [];

        this.mensagens = [];
    }
}