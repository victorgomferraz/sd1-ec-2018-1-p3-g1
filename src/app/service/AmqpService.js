import amqp from "amqplib/callback_api";
import * as config from '../config';

const QueueNames = {
    "REGISTRO_CONEXAO": "registro_conexao",
    "GRAVAR_MENSAGEM": "gravar_mensagem",
    "CHANNEL_MESSAGES": "messages_channel_[0]_user_[1]",
    "SAIR_CHAT": "exit_user",

    "ENTROU_NO_CHAT": "registered_user_[0]",
    "NICKS_CHANNEL": "nicks_channel_[0]",
};

let _instance = null;
let _connection = null;
let _channel = null;

export default class AmqpService {

    static get instance() {
        if (_instance === null) {
            initializeAmqp();
            _instance = new AmqpService();
        }
        return _instance;
    }

    static get connection() {
        return _connection;
    }

    static set connection(connection) {
        return _connection = connection;
    }

    static get channel() {
        return _channel;
    }

    static set channel(channel) {
        return _channel = channel;
    }

    sendConnectionRegisterEvent(message) {
        this.sendEvent(QueueNames.REGISTRO_CONEXAO, message);
    }

    sendSaveMessageEvent(message) {
        this.sendEvent(QueueNames.GRAVAR_MENSAGEM, message);
    }

    sendDisconnectEvent(user) {
        this.sendEvent(QueueNames.SAIR_CHAT, {idUser: user.id});
    }

    subscribeUser(user) {
        const queueName = QueueNames.CHANNEL_MESSAGES.replace('[0]', user.canal.nome).replace('[1]', user.id);
        this.subscribeQueue(queueName, (data) => {
            const {nick, msg, timestamp} = data;
            console.log(data);
            user.addMensagemByNickMsgAndTimestamp(nick, msg, timestamp, user.canal)
        });
    }

    subscribeEntrouNoChat(user) {
        const queueName = QueueNames.ENTROU_NO_CHAT.replace('[0]', user.id);
        this.subscribeQueue(queueName, () => {
            user.connected = true;
        });
    }

    subscribeNicksChannel(user) {
        const queueName = QueueNames.NICKS_CHANNEL.replace('[0]', user.canal.nome);
        this.subscribeQueue(queueName, (data) => {
            console.log('Recebi nicks da sala');
            console.log(data);
            user.canal.nicknames = Object.keys(data);
        });
    }

    subscribeQueue(queueName, callback) {
        AmqpService.channel.assertQueue(queueName, {durable: false});
        console.log("Registrado na fila " + queueName);
        AmqpService.channel.consume(queueName, (r) => {
            console.log("Fila " + queueName + " recebeu mensagem");
            console.log(queueName + " :" + r);
            console.log("---------------------------------------");
            callback(JSON.parse(r.content.toString()));
        }, {noAck: true});
    }

    sendEvent(event, message = null) {
        message = new Buffer(JSON.stringify(message));
        AmqpService.channel.assertQueue(event, {durable: false});
        AmqpService.channel.sendToQueue(event, message);
        console.log(" [app] Sent %s", message);
    }
}

function initializeAmqp() {
    amqp.connect(config.amqp.server, function (err, conn) {
        console.log(err ? 'Erro: Iniciar conexão com AMQP' : 'Conexão AMQP Criada com sucesso!');
        if (!err) {
            conn.createChannel(function (err, ch) {
                console.log(err ? 'Erro: Iniciar criar canal AMQP' : 'Canal AMQP criado com sucesso!');
                AmqpService.connection = conn;
                AmqpService.channel = ch;
            });
        }
    });
}