import User from "../model/User";
import Canal from "../model/Canal";

let _idGerador = 0;
let _usuarios = [];
export default class CacheService {

    static get idGerador() {
        return _idGerador;
    }

    static set idGerador(idGerador) {
        _idGerador = idGerador;
    }

    static get usuarios() {
        return _usuarios;
    }

    static set usuarios(usuarios) {
        _usuarios = usuarios;
    }

    static createNewUserByCookie(cookie) {
        this.validateCookie(cookie);
        const {servidor, nick, canal} = cookie;
        let channel = new Canal(canal);
        let user = this.findUserbyNick(nick);
        if (user === undefined) {
            this.idGerador = this.idGerador + 1;
            user = new User(this.idGerador, servidor, nick, channel);
            this.usuarios.push(user);
        }
        return user;
    }

    static validateCookie(cookie) {
        if (!cookie || !cookie.servidor || !cookie.nick || !cookie.canal)
            throw Error('Cookie incosistente');
    }

    static findUserbyNick(nick) {
        return this.usuarios.find((user) => user.nick.toLowerCase() === nick.toLowerCase());
    }

    static findUserbyId(id) {
        return this.usuarios.find((user) => user.id === parseInt(id));
    }

}