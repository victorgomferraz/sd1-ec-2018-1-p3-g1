import Mensagem from "../model/Mensagem";
import * as config from "../config"

export default class IrcService {

    static getStartUpMessage() {
        const {nick, message} = config.irc.defaultMessage;
        return new Mensagem(nick, message);
    }
}
