import * as path from "path";
import AmqpService from "../service/AmqpService";
import CacheService from "../service/CacheService";
import IrcService from "../service/IrcService";


export default function mainController(req, res) {
    const amqpServiceInstance = AmqpService.instance;
    let user = null;
    try {
        user = CacheService.createNewUserByCookie(req.cookies);
    } catch (e) {
        res.sendFile(path.join(__dirname, '../web/login.html'));
        return;
    }

    user.addMessage(IrcService.getStartUpMessage());
    res.cookie('id', user.id); // Seta o ID nos cookies do cliente
    amqpServiceInstance.sendConnectionRegisterEvent(user.toJsonWithoutMessagens());
    amqpServiceInstance.subscribeUser(user);
    amqpServiceInstance.subscribeEntrouNoChat(user);
    amqpServiceInstance.subscribeNicksChannel(user);
    res.sendFile(path.join(__dirname, '../web/index.html'));
}

export function getMessagensByUser(req, res) {
    let response = {
        isConnected: false,
        nicknames: [],
        messages:[]
    };
    if (req && req.cookies && req.cookies.id) {
        let user = CacheService.findUserbyId(req.cookies.id);
        if (user !== undefined) {
            response.messages = response.messages.concat(user.mensagens.map((m) => m.toJson()));
            response.nicknames = user.canal.nicknames;
            response.isConnected = user.connected;
            user.mensagens = [];
        }
    }
    res.append('Content-type', 'application/json');
    res.send(response);
}

export function registerAndSendMessageByUser(req, res) {
    const amqpServiceInstance = AmqpService.instance;
    if (req && req.cookies && req.cookies.id) {
        let user = CacheService.findUserbyId(req.cookies.id);
        if (user) {
            let message = user.sendMessage(req.body.msg);
            amqpServiceInstance.sendSaveMessageEvent(message.toJson())
        }
    }
    res.end();
}
export function exitChat(req, res) {
    const amqpServiceInstance = AmqpService.instance;
    if (req && req.cookies && req.cookies.id) {
        let user = CacheService.findUserbyId(req.cookies.id);
        if (user) {
            amqpServiceInstance.sendDisconnectEvent(user);
            res.clearCookie("nick");
            res.clearCookie("canal");
            res.clearCookie("servidor");
        }
    }
    res.end();
}

