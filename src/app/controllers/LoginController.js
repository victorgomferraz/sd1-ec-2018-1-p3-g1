export default function loginController(req, res) {
    res.cookie('nick', req.body.nome);
    if (req.body.canal && req.body.canal[0] !== '#') {
        req.body.canal = '#' + req.body.canal;
    }
    res.cookie('canal', req.body.canal);
    res.cookie('servidor', req.body.servidor);
    res.redirect('/');
};

