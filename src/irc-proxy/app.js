import AmqpService from "./service/AmqpService";
import CacheService from "./service/CacheService";
import IrcService from "./service/IrcService";

AmqpService.getInstance((amqpServiceInstance) => {
    amqpServiceInstance.subscribeConnectionRegister((msg) => {
        console.log('irc-proxy.js: recebeu registro de conexão');
        const {id, servidor, nick, canal} = msg;
        let client = IrcService.instance.createConnection(id, servidor, nick, canal);

        CacheService.addIrcClient(id, client);
        CacheService.addProxyByPos(id, CacheService.getIrcClientByPos(id));
    });

    amqpServiceInstance.subscribeSendMessage((msg) => {
        let ircClient = CacheService.getIrcClientByPos(msg.id);
        if (ircClient) {
            ircClient.say(msg.canal, msg.msg);
        }
    });

    amqpServiceInstance.subscribeExitChat((data) => {
        if (data && data.idUser) {
            let ircClient = CacheService.getIrcClientByPos(data.idUser);
            if (ircClient) {
                ircClient.disconnect();
                CacheService.removeProxyByPos(data.idUser);
            }
        }
    });
});
