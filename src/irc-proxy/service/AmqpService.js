import amqp from "amqplib/callback_api";
import * as config from '../config';

const QueueNames = {
    "REGISTRO_CONEXAO": "registro_conexao",
    "GRAVAR_MENSAGEM": "gravar_mensagem",
    "CHANNEL_MESSAGES": "messages_channel_[0]_user_[1]",
    "ENTROU_NO_CHAT": "registered_user_[0]",
    "NICKS_CHANNEL": "nicks_channel_[0]",
    "SAIR_CHAT": "exit_user"
};

let _instance = null;
let _connection = null;
let _channel = null;

function callbackDefault() {
}

export default class AmqpService {

    static get instance() {
        return _instance;
    }

    static getInstance(callback = callbackDefault) {
        if (_instance === null) {
            initializeAmqp(() => {
                _instance = new AmqpService();
                callback(_instance);
            });
        } else {
            callback(_instance);
        }
    }

    static get connection() {
        return _connection;
    }

    static set connection(connection) {
        return _connection = connection;
    }

    static get channel() {
        return _channel;
    }

    static set channel(channel) {
        return _channel = channel;
    }


    sendMessageToUserFromChannel(userId, canal, message) {
        const queueName = QueueNames.CHANNEL_MESSAGES.replace('[0]', canal).replace('[1]', userId);
        this.sendEvent(queueName, message);
    }

    subscribeConnectionRegister(callback) {
        this.subscribe(QueueNames.REGISTRO_CONEXAO, callback);
    }

    subscribeSendMessage(callback) {
        this.subscribe(QueueNames.GRAVAR_MENSAGEM, callback);
    }

    sendMessageRegisteredChat(userId, message) {
        this.sendEvent(QueueNames.ENTROU_NO_CHAT.replace('[0]', userId), message);
    }

    sendMessageNamesChannelChat(canal, message) {
        this.sendEvent(QueueNames.NICKS_CHANNEL.replace('[0]', canal), message);
    }

    subscribeExitChat(callback) {
        this.subscribe(QueueNames.SAIR_CHAT, callback);
    }


    subscribe(queueName, callback) {
        AmqpService.channel.assertQueue(queueName, {durable: false});
        console.log("Esperando dados fila: " + queueName);
        AmqpService.channel.consume(queueName, (r) => {
            console.log("Fila " + queueName + " recebeu mensagem");
            console.log(queueName + " :" + r.content.toString());
            console.log("---------------------------------------");
            callback(JSON.parse(r.content.toString()));

        }, {noAck: true});
    }

    sendEvent(event, message) {
        message = new Buffer(JSON.stringify(message));
        AmqpService.channel.assertQueue(event, {durable: false});
        AmqpService.channel.sendToQueue(event, message);
        console.log(" [app] Sent %s", message);
    }
}

function initializeAmqp(callback) {
    amqp.connect(config.amqp.server, function (err, conn) {
        console.log(err ? 'Erro: Iniciar conexão com AMQP' : 'Conexão AMQP Criada com sucesso!');
        if (!err) {
            conn.createChannel(function (err, ch) {
                console.log(err ? 'Erro: Iniciar criar canal AMQP' : 'Canal AMQP criado com sucesso!');
                AmqpService.connection = conn;
                AmqpService.channel = ch;
                callback();
            });
        }
    });
}