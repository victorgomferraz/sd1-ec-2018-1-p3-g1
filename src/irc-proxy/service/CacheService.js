let _proxies = [];
let _ircClients = [];
export default class CacheService {

    static get proxies() {
        return _proxies;
    }

    static set proxies(proxies) {
        _proxies = proxies;
    }

    static get ircClients() {
        return _ircClients;
    }

    static set ircClients(ircClients) {
        _ircClients = ircClients;
    }

    static addIrcClient(pos, ircClient) {
        this.ircClients[pos] = ircClient;
    }

    static getIrcClientByPos(pos) {
        return this.ircClients[pos];
    }


    static addProxyByPos(pos, ircClient) {
        this.proxies[pos] = ircClient;
    }

    static removeProxyByPos(pos) {
        delete this.proxies[pos];
    }

    static getProxyByPos(pos) {
        return this.proxies[pos];
    }

    static validateCookie(cookie) {
        if (!cookie || !cookie.servidor || !cookie.nick || !cookie.canal)
            throw Error('Cookie incosistente');
    }

    static findUserbyNick(nick) {
        return this.usuarios.find((user) => user.nick.toLowerCase() === nick.toLowerCase());
    }

    static findUserbyId(id) {
        return this.usuarios.find((user) => user.id === parseInt(id));
    }

}