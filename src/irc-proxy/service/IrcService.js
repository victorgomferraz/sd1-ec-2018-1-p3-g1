import irc from 'irc';
import AmqpService from "./AmqpService";

const IrcTopics = {
    "MESSAGE_CHANNEL": "message[0]",
    "NAMES_CHANNEL": "names[0]",
    "REGISTERED": "registered",
    "MOTD": "motd",
    "NAMES": "names",
    "TOPIC": "topic",
    "JOIN": "join",
    "JOIN_CHANNEL": "join[0]",
    "PART": "part",
    "PART_CHANNEL": "part[0]",
    "QUIT": "quit",
    "KICK": "kick",
    "KICK_CHANNEL": "kick[0]",
    "KILL": "kill",
    "MESSAGE": "message",
    "SELF_MESSAGE": "selfMessage",
    "NOTICE": "notice",
    "PM": "pm",
    "NICK": "nick",
    "INVITE": "invite",
    "WHOIS": "whois",
    "ERROR": "error"
};


let _instance = null;
export default class IrcService {
    static get instance() {
        if (_instance === null) {
            _instance = new IrcService();
        }
        return _instance;
    }

    static set instance(instance) {
        _instance = instance;
    }

    createConnection(idUser, servidor, nick, canal) {
        let client = new irc.Client(
            servidor,
            nick,
            {channels: [canal]}
        );
        client.addListener(IrcTopics.MESSAGE_CHANNEL.replace('[0]', canal), (from, message) => this.messageChannelHandler(idUser, from, message, canal));
        client.addListener(IrcTopics.NAMES_CHANNEL.replace('[0]', canal), (nicks) => this.namesChannelHandler(canal, nicks));
        client.addListener(IrcTopics.REGISTERED, (msg) => this.registeredHandler(idUser, msg));
        client.addListener(IrcTopics.MOTD, this.motdHandler);
        client.addListener(IrcTopics.NAMES, this.namesHandler);
        client.addListener(IrcTopics.TOPIC, this.topicHandler);
        client.addListener(IrcTopics.JOIN, this.joinHandler);
        client.addListener(IrcTopics.JOIN_CHANNEL, this.joinChannelHandler);
        client.addListener(IrcTopics.PART, this.partHandler);
        client.addListener(IrcTopics.PART_CHANNEL, this.partChannelHandler);
        client.addListener(IrcTopics.QUIT, this.quitHandler);
        client.addListener(IrcTopics.KICK, this.kickHandler);
        client.addListener(IrcTopics.KICK_CHANNEL, this.kickChannelHandler);
        client.addListener(IrcTopics.KILL, this.killHandler);
        client.addListener(IrcTopics.MESSAGE, this.messageHandler);
        client.addListener(IrcTopics.SELF_MESSAGE, this.selfMessageHandler);
        client.addListener(IrcTopics.NOTICE, this.noticeHandler);
        client.addListener(IrcTopics.PM, this.pmHandler);
        client.addListener(IrcTopics.NICK, this.nickHandler);
        client.addListener(IrcTopics.INVITE, this.inviteHandler);
        client.addListener(IrcTopics.WHOIS, this.whoisHandler);
        client.addListener(IrcTopics.ERROR, this.errorHandler);
        return client;
    }

    messageChannelHandler(idUser, from, message, canal) {
        console.log(from + ' => ' + canal + ': ' + message);
        AmqpService.instance.sendMessageToUserFromChannel(idUser, canal, {
            "timestamp": Date.now(),
            "nick": from,
            "msg": message
        });
    }

    /**
     * Emitted when the server sends the initial 001 line, indicating you’ve connected to the server. See the raw event for details on the message object.
     * @param msg
     */
    registeredHandler(idUser, msg) {
        console.log('registered:');
        console.log(msg);
        AmqpService.instance.sendMessageRegisteredChat(idUser, msg);

    }

    /**
     * Emitted when the server sends the message of the day to clients.
     * @param motd
     */
    motdHandler(motd) {
        console.log('motd:');
        console.log(motd);

    }

    /**
     * Emitted when the server sends a list of nicks for a channel (which happens immediately after joining and on request. The nicks object passed to the callback is keyed by nick names, and has values ‘’, ‘+’, or ‘@’ depending on the level of that nick in the channel.
     * @param channel
     * @param nicks
     */
    namesHandler(channel, nicks) {
        console.log('names:');
        console.log(channel, nicks);

    }

    /**
     * As per ‘names’ event but only emits for the subscribed channel.
     * @param nicks
     */
    namesChannelHandler(channel, nicks) {
        console.log('namesChannel:');
        console.log(nicks);
        AmqpService.instance.sendMessageNamesChannelChat(channel, nicks);
    }

    /**
     * Emitted when the server sends the channel topic on joining a channel, or when a user changes the topic on a channel. See the raw event for details on the message object.
     *
     * @param channel
     * @param topic
     * @param nick
     * @param message
     */
    topicHandler(channel, topic, nick, message) {
        console.log('topic');
        console.log(channel, topic, nick, message);
    }

    /**
     *Emitted when a user joins a channel (including when the client itself joins a channel). See the raw event for details on the message object.
     *
     * @param channel
     * @param nick
     * @param message
     */
    joinHandler(channel, nick, message) {
        console.log('join');
        console.log(channel, nick, message);
    }

    /**
     * As per ‘join’ event but only emits for the subscribed channel. See the raw event for details on the message object.
     * @param nick
     * @param message
     */
    joinChannelHandler(nick, message) {
        console.log('joinChannel');
        console.log(nick, message);
    }

    /**
     * Emitted when a user parts a channel (including when the client itself parts a channel). See the raw event for details on the message object.
     *
     * @param channel
     * @param nick
     * @param reason
     * @param message
     */
    partHandler(channel, nick, reason, message) {
        console.log('part');
        console.log(channel, nick, reason, message);
    }

    /**
     * As per ‘part’ event but only emits for the subscribed channel. See the raw event for details on the message object.
     * @param nick
     * @param reason
     * @param message
     */
    partChannelHandler(nick, reason, message) {
        console.log('partChannel');
        console.log(nick, reason, message);
    }

    /**
     * Emitted when a user disconnects from the IRC, leaving the specified array of channels. See the raw event for details on the message object.
     * @param nick
     * @param reason
     * @param channels
     * @param message
     */
    quitHandler(nick, reason, channels, message) {
        console.log('quit');
        console.log(nick, reason, channels, message);
    }

    /**
     * Emitted when a user is kicked from a channel. See the raw event for details on the message object.
     * @param channel
     * @param nick
     * @param by
     * @param reason
     * @param message
     */
    kickHandler(channel, nick, by, reason, message) {
        console.log('kick');
        console.log(channel, nick, by, reason, message);
    }

    /**
     * As per ‘kick’ event but only emits for the subscribed channel. See the raw event for details on the message object.
     * @param nick
     * @param by
     * @param reason
     * @param message
     */
    kickChannelHandler(nick, by, reason, message) {
        console.log('kick');
        console.log(nick, by, reason, message);
    }

    /**
     * Emitted when a user is killed from the IRC server. channels is an array of channels the killed user was in which are known to the client. See the raw event for details on the message object.
     * @param nick
     * @param reason
     * @param channels
     * @param message
     */
    killHandler(nick, reason, channels, message) {
        console.log('kill');
        console.log(nick, reason, channels, message);
    }

    /**
     * Emitted when a message is sent. to can be either a nick (which is most likely this clients nick and means a private message), or a channel (which means a message to that channel). See the raw event for details on the message object.
     * @param nick
     * @param to
     * @param text
     * @param message
     */
    messageHandler(nick, to, text, message) {
        console.log('message');
        console.log(nick, to, text, message);
    }

    /**
     * Emitted when a message is sent from the client. to is who the message was sent to. It can be either a nick (which most likely means a private message), or a channel (which means a message to that channel).
     * @param to
     * @param text
     */
    selfMessageHandler(to, text) {
        console.log('selfMessage');
        console.log(to, text);
    }

    /**
     * Emitted when a notice is sent. to can be either a nick (which is most likely this clients nick and means a private message), or a channel (which means a message to that channel). nick is either the senders nick or null which means that the notice comes from the server. See the raw event for details on the message object.
     * @param nick
     * @param to
     * @param text
     * @param message
     */
    noticeHandler(nick, to, text, message) {
        console.log('notice');
        console.log(nick, to, text, message);
    }

    /**
     * As per ‘message’ event but only emits when the message is direct to the client. See the raw event for details on the message object.
     * @param nick
     * @param text
     * @param message
     */
    pmHandler(nick, text, message) {
        console.log('PM');
        console.log(nick, text, message);
    }

    /**
     * Emitted when a user changes nick along with the channels the user is in. See the raw event for details on the message object.
     * @param oldnick
     * @param newnick
     * @param channels
     * @param message
     */
    nickHandler(oldnick, newnick, channels, message) {
        console.log('nick');
        console.log(oldnick, newnick, channels, message);
    }

    /**
     * Emitted when the client receives an /invite. See the raw event for details on the message object.
     * @param channel
     * @param from
     * @param message
     */
    inviteHandler(channel, from, message) {
        console.log('invite');
        console.log(channel, from, message);
    }

    /**
     * Emitted whenever the server finishes outputting a WHOIS response. The information should look something like:
     * @param info
     */
    whoisHandler(info) {
        console.log('whois');
        console.log(info);
    }

    errorHandler(err) {
        console.log('error: ', err);
    }

}

