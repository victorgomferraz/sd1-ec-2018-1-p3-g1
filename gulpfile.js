const gulp = require('gulp');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const runSequence = require('run-sequence');

const IRC = 'irc-proxy/';
const APP = 'app/';
const bases = {
    appMain: 'src/' + APP,
    appIrc: 'src/' + IRC,
    distMain: 'dist/' + APP,
    distIrc: 'dist/' + IRC,
};

gulp.task('clean-app', function () {
    return gulp.src(bases.distMain).pipe(clean());
});

gulp.task('js-app', () => {
    return gulp.src(['./' + bases.appMain + '/**/*.js', '!./' + bases.appMain + '/web/**'])
        .pipe(babel({
            presets: ['es2016']
        }))
        .pipe(gulp.dest('./' + bases.distMain));
});


gulp.task('copy-web-app', function () {
    return gulp.src('./' + bases.appMain + '/web/**').pipe(gulp.dest(bases.distMain + '/web'));

});

gulp.task('copy-json-app', function () {
    return gulp.src('./' + bases.appMain + '/**/*.json').pipe(gulp.dest('./' + bases.distMain));

});

gulp.task('clean-irc', function () {
    return gulp.src(bases.distIrc).pipe(clean());
});


gulp.task('js-irc', () => {
    return gulp.src(['./' + bases.appIrc + '/**/*.js'])
        .pipe(babel({
            presets: ['es2016']
        }))
        .pipe(gulp.dest('./' + bases.distIrc));
});


gulp.task('copy-json-irc', function () {
    return gulp.src('./' + bases.appIrc + '/**/*.json').pipe(gulp.dest('./' + bases.distIrc));

});


gulp.task('build-app', ()=>{
    runSequence('clean-app', 'js-app', 'copy-web-app', 'copy-json-app')
});

gulp.task('build-irc', ()=>{
    runSequence('clean-irc', 'js-irc', 'copy-json-irc')
});


gulp.task('default', ['build-app', 'build-irc']);