Projeto 3 das disciplinas de Aplicações Distribuídas (SI) e Sistemas Distribuídos (EC) do semestre 2018-1

<h2>Como Instalar</h2>
Para executar este projeto é necessário ter instalado o framework NODEJS, junto com o gerenciador de pacotes npm(instalado por padrão com o NODEJS)
E o serviço Rabbitmq para rodar localmente.
- [node](https://nodejs.org/en/)
- [npm](https://www.npmjs.com/)
- [rabbitmq](https://www.rabbitmq.com/)

<h2>Download do repositório e execução da aplicação</h2>
Para realizar instalação primeiramente é necessário baixar o repositório.
* Clonando repositório pelo git

Via terminal clone o repositório:

````
git clone https://gitlab.com/victorgomferraz/sd1-ec-2018-1-p3-g1.git
````
* Baixando repositório via página do projeto

* Acesse a pasta da aplicação
via terminal (linux, macOS e Windows)

````
     cd sd1-ec-2018-1-p3-g1
````

<h3>Instalação das dependências do projeto</h3>

````
     npm install
````

<h2>Iniciar aplicação</h2>
O RabbitMQ já deve estar iniciado.
O Projeto é dividido no Proxy principal e no Proxy IRC.

Para iniciar o proxy princial rode:

````
     npm run start-app
`````

Para iniciar o Proxy IRC rode:

````
     npm run start-irc
`````

No browser de sua preferência, digite na barra de endereço

````
     localhost:3000
````

<h2>Usando servidor RabbitMQ Remoto</h2>

Os 2 Projetos Proxy Principal e Proxy IRC possuem arquivos `config.json`
Basta alterar `amqp.server` em ambos os projetos para o servidor remoto.

<h2>Testando</h2>

Para testar com outro cliente, uma sugestão seria [kiwiirc.com](https://kiwiirc.com/client/irc.freenode.net),
acesse a mesma sala e interaja entre os chats.
